.templates_sha: &templates_sha 79c325922670137e8f0a4dc5f6f097e0eb57c1af

include:
  - project: 'freedesktop/ci-templates'
    ref: *templates_sha
    file: '/templates/debian.yml'

variables:
  FDO_UPSTREAM_REPO: gdesmott/karapulse

stages:
  - "container"
  - "lint"
  - "test"
  - "extras"

.debian:
  variables:
    # Update this tag when you want to trigger a rebuild
    FDO_DISTRIBUTION_TAG: '2022-01-12.0'
    # Uncomment if you want to always rebuild the container, useful when hacking on it
    #FDO_FORCE_REBUILD: 1
    FDO_DISTRIBUTION_VERSION: 10
    FDO_DISTRIBUTION_PACKAGES: >-
      appstream-util
      build-essential
      curl
      desktop-file-utils
      gettext
      git
      gstreamer1.0-gl
      gstreamer1.0-gtk3
      gstreamer1.0-libav
      gstreamer1.0-plugins-bad
      gstreamer1.0-plugins-good
      gstreamer1.0-tools
      gstreamer1.0-x
      libglib2.0-dev
      libgstreamer-plugins-base1.0-dev
      libgtk-3-dev
      libsqlite3-dev
      libssl-dev
      ninja-build
      pkg-config
      python3-dev
      python3-pip
      python3-setuptools
      wget
    FDO_DISTRIBUTION_EXEC: >-
      ci/install-rust.sh stable &&
      pip3 install meson &&
      curl -sL https://deb.nodesource.com/setup_12.x | bash - &&
      apt-get install -y --no-install-recommends nodejs
  before_script:
    - source ./ci/env.sh
    - mkdir .cargo && echo -e "[net]\ngit-fetch-with-cli = true" > .cargo/config
    # If cargo exists assume we probably will want to update
    # the lockfile
    - |
      if command -v cargo; then
        cargo generate-lockfile --color=always
        cargo update --color=always
      fi

container:
  extends:
    - .debian
    - .fdo.container-build@debian
  stage: container

.debian_img:
  extends:
    - .debian
    - .fdo.distribution-image@debian

variables:
  GNOME_RUNTIME_IMAGE: 'registry.gitlab.gnome.org/gnome/gnome-runtime-images/gnome:3.38'

.cache_setup:
  extends: .debian_img
  cache:
    key: "target"
    paths:
      - build/target/
  cache:
    key: "npm"
    paths:
      - web/node_modules/

test:
  stage: "test"
  extends: '.cache_setup'
  script:
    - rustc --version
    - meson build -D cdg-plugin=true -D prefix=$PWD/install -D profile=development
    - ninja -C build install
    - meson test -C build -v

rustfmt:
  stage: "lint"
  extends: .debian_img
  script:
    - meson build -D cdg-plugin=true -D prefix=$PWD/install -D profile=development
    - cargo fmt --version
    - cargo fmt -- --color=always --check

clippy:
  stage: 'extras'
  extends: .debian_img
  script:
    - meson build -D cdg-plugin=true -D prefix=$PWD/install -D profile=development
    - cargo clippy --version
    - cargo clippy --color=always --all --all-features -- -D warnings
  extends: '.cache_setup'

outdated:
  stage: 'extras'
  extends: .debian_img
  only:
    - schedules
    - release
  script:
    - cargo outdated -v --root-deps-only --exit-code 1
  extends: '.cache_setup'

flatpak:
    image: $GNOME_RUNTIME_IMAGE
    stage: "test"
    only:
      - flatpak
    timeout: '120min'
    variables:
        MANIFEST_PATH: "build-aux/org.karapulse.KarapulseDevel.json"
        APP_ID: "org.karapulse.KarapulseDevel"
        BUNDLE: "karapulse-nightly.flatpak"

    script:
        - flatpak-builder app ${MANIFEST_PATH}
        - flatpak build-export repo app
        - flatpak build-bundle repo ${BUNDLE} ${APP_ID}

    artifacts:
        paths:
            - $BUNDLE
        expire_in: 1 days

    cache:
        key: "flatpak"
        paths:
          - .flatpak-builder/downloads/
          - .flatpak-builder/git/
          - target/
          - target_test/
