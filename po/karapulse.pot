# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the karapulse package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: karapulse\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-02-10 17:42+0530\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/org.karapulse.Karapulse.appdata.xml.in.in:7
#: data/org.karapulse.Karapulse.desktop.in.in:3
#: data/org.karapulse.Karapulse.desktop.in.in:4
msgid "Karapulse"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:8
msgid "Karaoke player with built-in web remote client"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:10
#: data/org.karapulse.Karapulse.desktop.in.in:5
msgid ""
"Karapulse is a Linux karaoke player supporting CDG/MP3 as well as video "
"files. It provides a self-served web application that singers can use with "
"their phone to search for and queue their favorite songs."
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:11
msgid ""
"Once installed you'll need to index your songs collection. This can take a "
"while if you have a lot of songs but needs to be done only once. To do so "
"run the following command with the directory containing your songs ($HOME/"
"karaoke in the example):"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:13
msgid ""
"flatpak run --command=karapulse-manage-db org.karapulse.Karapulse add-dir "
"$HOME/karaoke"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:18
msgid "Player rendering video"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:22
msgid "Player rendering CDG"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:26
msgid "Searching song using the mobile client"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:35
msgid "Guillaume Desmottes"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:44
msgid "web: synchronize play-pause button with server status"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:45
msgid "Add translation support (fr)"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:46
msgid "Hide cursor in fullscreen"
msgstr ""

#: data/org.karapulse.Karapulse.appdata.xml.in.in:47
msgid "Fix queue restoring"
msgstr ""

#: data/org.karapulse.Karapulse.desktop.in.in:10
msgid "Karaoke;Video;Series;Player;"
msgstr ""

#. Translators: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/org.karapulse.Karapulse.desktop.in.in:12
msgid "@icon@"
msgstr ""

#: src/player.rs:323
msgid "Waiting for songs"
msgstr ""
