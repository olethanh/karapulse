// Copyright (C) 2019 Guillaume Desmottes <guillaume@desmottes.be>
//
// This program is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see <https://www.gnu.org/licenses/>.

use std::collections::{HashMap, VecDeque};

use crate::karapulse::Song;

#[derive(Debug, Clone)]
pub struct Item {
    pub user: String,
    pub song: Song,
    pub history_id: Option<i64>,
}

impl Item {
    fn new(user: &str, song: Song, history_id: Option<i64>) -> Self {
        let user = user.to_string();

        Self {
            user,
            song,
            history_id,
        }
    }
}

#[derive(Debug)]
pub struct Queue {
    songs: VecDeque<Item>,
    last_pop: Option<String>, // owner of the last item poped from the queue
}

impl Queue {
    pub fn new() -> Queue {
        Queue {
            songs: VecDeque::new(),
            last_pop: None,
        }
    }

    #[allow(dead_code)]
    fn len(&self) -> usize {
        self.songs.len()
    }

    fn find_place(&self, user: &str) -> usize {
        /* Iterate the queue from the front to the tail. Take the place of
         * the second element owned by someone having more than one element
         * since the last element owned by @user. */
        let mut counts: HashMap<&str, u32> = HashMap::new();

        /* Count the last item popep out of the queue */
        if let Some(u) = &self.last_pop {
            counts.insert(u, 1);
        }

        for (i, elt) in self.songs.iter().enumerate() {
            let owner = &elt.user;

            if owner == user {
                // current element has been added by @user, reset counters
                counts = HashMap::new();
                continue;
            }

            let count = match counts.get_mut::<str>(owner) {
                Some(count) => {
                    *count += 1;
                    *count
                }
                None => {
                    counts.insert(owner, 1);
                    1
                }
            };

            if count > 1 {
                return i;
            }
        }

        self.songs.len()
    }

    pub fn add(&mut self, user: &str, song: Song, history_id: Option<i64>) {
        let song = Item::new(user, song, history_id);

        let idx = self.find_place(user);
        self.songs.insert(idx, song);
    }

    pub fn next(&mut self) -> Option<Item> {
        let item = self.songs.pop_front();

        if let Some(ref i) = item {
            self.last_pop = Some(i.user.clone())
        }

        item
    }

    pub fn snapshot(&self) -> Vec<Item> {
        let mut v = Vec::new();
        self.songs.iter().for_each(|s| v.push(s.clone()));
        v
    }

    #[allow(dead_code)]
    pub fn iter(&self) -> std::collections::vec_deque::Iter<Item> {
        self.songs.iter()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::path::{Path, PathBuf};

    #[test]
    fn add() {
        let mut q = Queue::new();
        assert_eq!(q.len(), 0);

        let p = PathBuf::from("/first");
        q.add("a", Song::Path(p), None);
        assert_eq!(q.len(), 1);

        let p = PathBuf::from("/second");
        q.add("b", Song::Path(p), None);
        assert_eq!(q.len(), 2);

        let snapshot = q.snapshot();
        assert_eq!(snapshot.len(), 2);

        let s = q.next().unwrap();
        assert_eq!(q.len(), 1);
        assert_eq!(s.song.path(), Path::new("/first"));

        let s = q.next().unwrap();
        assert_eq!(q.len(), 0);
        assert_eq!(s.song.path(), Path::new("/second"));

        let s = q.next();
        assert!(s.is_none());
    }

    fn check_order(check: &[&str], q: &Queue) {
        let tmp: Vec<String> = q
            .iter()
            .map(|x| {
                let path = x.song.path();
                path.to_str().unwrap().to_string()
            })
            .collect();
        assert_eq!(tmp, check);
    }

    #[test]
    fn ordering_simple() {
        let mut q = Queue::new();
        q.add("a", Song::Path(Path::new("/a1").to_path_buf()), None);
        q.add("a", Song::Path(Path::new("/a2").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b1").to_path_buf()), None);
        check_order(&["/a1", "/b1", "/a2"], &q);
    }

    #[test]
    fn ordering_multi() {
        let mut q = Queue::new();
        q.add("a", Song::Path(Path::new("/a1").to_path_buf()), None);
        q.add("a", Song::Path(Path::new("/a2").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b1").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b2").to_path_buf()), None);
        q.add("a", Song::Path(Path::new("/a3").to_path_buf()), None);
        q.add("c", Song::Path(Path::new("/c1").to_path_buf()), None);
        q.add("a", Song::Path(Path::new("/a4").to_path_buf()), None);
        check_order(&["/a1", "/b1", "/c1", "/a2", "/b2", "/a3", "/a4"], &q);
    }

    #[test]
    fn ordering_bug() {
        let mut q = Queue::new();
        q.add("a", Song::Path(Path::new("/a1").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b1").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b2").to_path_buf()), None);
        q.add("a", Song::Path(Path::new("/a2").to_path_buf()), None);
        check_order(&["/a1", "/b1", "/a2", "/b2"], &q);
    }

    #[test]
    fn ordering_pop() {
        let mut q = Queue::new();
        q.add("a", Song::Path(Path::new("/a1").to_path_buf()), None);
        /* File is played right away */
        q.next();
        q.add("a", Song::Path(Path::new("/a2").to_path_buf()), None);
        q.add("b", Song::Path(Path::new("/b1").to_path_buf()), None);
        check_order(&["/b1", "/a2"], &q);
    }
}
