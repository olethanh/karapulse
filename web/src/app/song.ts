export class Song {
  id: number;
  artist: string;
  title: string;
  length: number;
}
