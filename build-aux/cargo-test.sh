#!/bin/bash

MESON_BUILD_ROOT="$1"
MESON_SOURCE_ROOT="$2"
PROFILE=$3
EXTRA_ARGS="$4"
OFFLINE="$5"

export CARGO_TARGET_DIR="$MESON_BUILD_ROOT"/target

if [[ $PROFILE = "Devel" ]]
then
	RELEASE="--verbose"
else
	RELEASE="--release"
fi

echo $CARGO_TARGET_DIR

cargo test --no-fail-fast $OFFLINE $RELEASE --manifest-path \
    "$MESON_SOURCE_ROOT"/Cargo.toml $EXTRA_ARGS
