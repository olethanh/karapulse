#!/bin/bash

export MESON_BUILD_ROOT="$1"
export MESON_SOURCE_ROOT="$2"
export CARGO_TARGET_DIR="$MESON_BUILD_ROOT"/target
export SHORTWAVE_LOCALEDIR="$4"
export EXTRA_ARGS="$6"
export BIN="$7"
export OFFLINE="$8"

if [[ $5 = "Devel" ]]
then
    echo "DEBUG MODE"
    cargo build $OFFLINE --bins --tests --manifest-path \
        "$MESON_SOURCE_ROOT"/Cargo.toml --verbose $EXTRA_ARGS && \
        cp "$CARGO_TARGET_DIR"/debug/$7 $3
else
    echo "RELEASE MODE"
    cargo build $OFFLINE --bins --tests --manifest-path \
        "$MESON_SOURCE_ROOT"/Cargo.toml --release $EXTRA_ARGS && \
        cp "$CARGO_TARGET_DIR"/release/$7 $3
fi
